from django.urls import path

from . import views

app_name = 'story'

urlpatterns = [
    path('index', views.index3, name='index')
]