from django import forms

from .models import MataKuliah

class FormMataKuliah(forms.ModelForm):
    nama = forms.CharField()
    dosen =  forms.CharField()
    jumlahSKS = forms.IntegerField()
    deskripsi = forms.CharField()
    tahun = forms.CharField()
    ruangKelas = forms.CharField()

    class Meta:
        model = MataKuliah
        fields = '__all__'