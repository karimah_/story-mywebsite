from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    nama = models.CharField(max_length=50)
    dosen =  models.CharField(max_length=50)
    jumlahSKS = models.PositiveIntegerField()
    deskripsi = models.CharField(max_length=50)
    tahun = models.CharField(max_length=50)
    ruangKelas = models.CharField(max_length=20)

    def __str__(self):
        return self.nama