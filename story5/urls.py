from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('ListMataKuliah', views.Utama, name='jadwal_list'),
    path('FormMataKuliah',views.formMatkul, name='jadwal_form'),
    path('<int:id>',views.matkulDetail),
    path('deleteMataKuliah/<str:id>', views.deleteMatkul,name="jadwal_delete")
]