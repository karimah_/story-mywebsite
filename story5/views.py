from django.shortcuts import render,get_object_or_404, redirect
from django.http import HttpResponse, Http404
from .models import MataKuliah
from .forms import FormMataKuliah

# Create your views here.
def Utama(request):
    matahkuliahs = MataKuliah.objects.all()
    return render(request, 'story5/jadwal_list.html',{'matakuliahs':matahkuliahs})

def formMatkul(request):
    form = FormMataKuliah()
    if request.method == 'POST':
        form = FormMataKuliah(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/ListMataKuliah')
    return render(request, 'story5/jadwal_form.html',{'form':form})

def matkulDetail(request,id):
    matakuliah = MataKuliah.objects.get(id=id)
    return render(request,'story5/jadwal_details.html',{'matakuliah':matakuliah})

def deleteMatkul(request,id):
    item = MataKuliah.objects.get(id=id)
    if request.method == 'POST' :
        item.delete()
        return redirect('/ListMataKuliah')
    return render(request, 'story5/jadwal_delete.html',{'item':item})
