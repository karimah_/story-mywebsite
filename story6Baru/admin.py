from django.contrib import admin
from .models import KegiatanSaya, Participant

# Register your models here.
admin.site.register(KegiatanSaya)
admin.site.register(Participant)
