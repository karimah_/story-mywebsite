from django.urls import path
from . import views

app_name = 'story7'

urlpatterns = [
    path('homepage', views.homepage, name='homepage'),
    path('story7', views.story7Views, name='story7'),
    # path()
]
