from django.shortcuts import render

# Create your views here.

def homepage(request):
    return render(request, 'homepage.html')

def story7Views(request):
    return render(request, 'story7.html')

# def back(request):
#     return render(request, 'home/index1.html')
