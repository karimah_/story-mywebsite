from django.urls import path, include
from . import views

app_name = 'story9'

urlpatterns = [
    path('login/',views.logIn, name='login'),
    path('signup/', views.signUp, name='signup'),
    path('logout/', views.logOut, name='logout'),
    # path('', include('django.contrib.auth.urls'), name='django-auth'),

]
